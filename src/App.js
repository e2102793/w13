import "./App.css";

let weekdays = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];
function App() {
  return (
    <div className="App">
      <h1> W13 weekdays in table </h1>
      <table>
        <thead>
          <tr>
            <th>Weekday</th>
          </tr>
        </thead>
        <tbody>
          {weekdays.map((days) => (
            <tr key={days}>
              <td>{days}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default App;
